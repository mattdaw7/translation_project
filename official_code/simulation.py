import bom
import numpy as np


def value(x):
    """
    This is the value function decided on that determines how much value we will
    gain based on how well the word is learned.

    If the word is 95% learned, then value will be very low
    If the word is 5% learned, the value will be low, but not as low as 95%
    The sweet spot is in the middle
    """
    return -1 * (x-1) * (x+0.1)


def get_next_word(predicted_state, importance):
    """
    Simple function that uses our predicted state and importance
    to find the optimal next word to study

    Predicted state and importance are both dictionaries of words to numbers
    """
    best_word = None
    best_contribution = 0

    for word in importance:
        word_understanding = 0
        if word in predicted_state:
            word_understanding = predicted_state[word]

        contribution = value(word_understanding) * importance[word]
        if contribution > best_contribution:
            best_word = word
            best_contribution = contribution

    return best_word


def mock_study_session(word_to_study):
    """
    Returns a boolean as to whether the learner correctly guessed the word.
    This implementation sucks big time. Ideal implementation would be a class object,
    that keeps its own state hidden, and then randomly chooses if they got the word 
    right or not based on that state. Something like

    correct = my_artificial_learner.study(word)
    """
    return np.random.rand() < 0.5


def mock_update_state(predicted_state, word_studied, correct):
    """
    Super naive way to update the state, doesn't even look at whether they correctly guessed.
    """
    if word_studied not in predicted_state:
        predicted_state[word_studied] = .1
    else:
        predicted_state[word_studied] += 0.5 * \
            (1 - predicted_state[word_studied])

    return predicted_state


if __name__ == '__main__':
    print("--- Book of Mormon Language Learning Simulation ---")
    num_words_study = input("Please enter the number of words to study: ")

    if num_words_study.isdigit():
        num_words_study = int(num_words_study)
    else:
        print("You suck at entering numbers. Defaulting to 100.")
        num_words_study = 100

    importance = bom.get_bom_importance()
    predicted_state = dict()

    for _i in range(0, num_words_study):
        word = get_next_word(predicted_state, importance)
        print(word)

        # Mock Study Session
        correct = mock_study_session(word)

        # Mock State Adjustment
        predicted_state = mock_update_state(predicted_state, word, correct)
