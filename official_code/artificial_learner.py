import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
from datetime import timedelta

from word_class import word
from word_class import word_study_incident_previous_session
from word_class import word_study_incident_current_session

from parent_learner_object import parent_learner_object

class artificial_learner(parent_learner_object):
    def __init__(self, num_words, foreign_word_to_learn=None, native_language_equivelent=None, native_language_definition=None, word_importance_score=None, hours_to_break_session=1.5):
        """
        This class acts to estimate what our learner knows and predict what study word would best improve the
        student's learning
        :param num_words: number of words in dictionary
        foreign_word_to_learn: foreign word we wish to learn in it's foreign language
        native_language_equivelent: translation of word in user's target language
        native_language_definition: definition of foreign_word_to_learn typed out in user's native language
        word_importance_score: score value of utility gained in learning word
        hours_to_break_session: if this much time elapses between word study incidences then we start a new session
        """
        parent_learner_object.__init__(self, num_words=num_words, foreign_word_to_learn=foreign_word_to_learn, native_language_equivelent=native_language_equivelent, native_language_definition=native_language_definition, word_importance_score=word_importance_score, hours_to_break_session=hours_to_break_session)

        # be is a positive number
        b = .1
        # c is  a number strictly greater than zero but generally should be less than one
        c = .1
        self.main_learning_function = lambda x: min(-b * (x - 1) * (x + c) + x, 1)


    def degrade_short_term_memory(self, word_id):
        """
        This function will degrade our short term memory of a word given how long it has been since we've studied it
        :param word_id: word id of word to degrade our memory for
        :return: None
        """
        # this is all the informaiton you really need
        words_reviewed_in_current_session = self.words[word_id].words_reviewed_in_current_session
        words_reviewed_in_previous_session = self.words[word_id].words_reviewed_in_previous_session

        # you may also just concatenate this list to words_reviewed_in_current_session if that's easier to work with
        words_reviewed_previous_session_unformatted = self.words[word_id].words_reviewed_previous_session_unformatted

        constructed_states, time_studying_word, correct_guesses = self.get_state_information(word_id, use_only_current_session=True)

        ### TODO do the math to update our estimate for how well a know understands the word given the results of this study instance


    def study_word(self, word_id):
        """
        This function will update our actual understanding of the word given that we study it
        :param word_id: word id of word to study
        :return: None
        """
        # this information is only the information from current state
        # this is all the informaiton you really need
        words_reviewed_in_current_session = self.words[word_id].words_reviewed_in_current_session
        words_reviewed_in_previous_session = self.words[word_id].words_reviewed_in_previous_session

        # you may also just concatenate this list to words_reviewed_in_current_session if that's easier to work with
        words_reviewed_previous_session_unformatted = self.words[word_id].words_reviewed_previous_session_unformatted

        constructed_states, time_studying_word, correct_guesses = self.get_state_information(word_id, use_only_current_session=True)

        ### TODO, do the math to update the state space of how well we know this word given that we study it
        # this is a function that determines how much the short term memory is increased by seeing a word in this
        # study session as a function of the number of times a word is seen. This will be affected by the long term memory
        # and the time it's been since we've seen the word
        # short_term_memory_update = lambda times_seen: (np.exp(-0.1 * times_seen) * times_seen + 0.1) / (10 * np.exp(-1) + 0.1)
        # this is a function that determines how much the long term memory is increased by studying a word in this
        # study session as a function of the short term memory
        num_times_word_seen_in_current_session = len(words_reviewed_in_current_session)
        # get the most recent period of time since we've studied this word
        most_recent_time_period = words_reviewed_in_current_session[0].time_since_last_instance
        for word in words_reviewed_in_current_session[1:]:
            if word.time_since_last_instance <= most_recent_time_period:
                most_recent_time_period = word.time_since_last_instance
        hours_to_break_session_seconds = self.hours_to_break_session * 60 * 60
        # if the most_recent_time_period is greater than the hours to break session, we set time_multiplier to be 0.01
        if most_recent_time_period.total_seconds() > hours_to_break_session_seconds:
            time_multiplier = 0.01
        # otherwise set time_multiplier to be 1 - (most_recent_time_period.total_seconds() / hours_to_break_session_seconds)
        # this prioritizes short term memory gain when it hasn't been too long since we've studied the word
        else:
            time_multiplier = 1 - (most_recent_time_period.total_seconds() / hours_to_break_session_seconds)
        # get an initial probability for the current word in short term memory
        short_term_memory_prob = self.main_learning_function(num_times_word_seen_in_current_session)
        # if we've never seen this word before, that is most_recent_time_period is zero, then
        # multiply the short_term_memory_prob by an appropriate constant to account for this
        if most_recent_time_period.total_seconds() == 0:
            short_term_memory_prob = 0.1 * short_term_memory_prob
        # if we've seen this word before, multiply the short_term_memory_prob by the long_term_recall_prob and the time_multiplier
        else:
            short_term_memory_prob = self.words[word_id].long_term_recall_prob * time_multiplier * short_term_memory_prob
        # update the short_term_recall_prob for the current word being studied
        self.words[word_id].short_term_recall_prob = short_term_memory_prob
        # update the long_term_recall_prob for the current word being studied
        # this is a fraction of the short_term_memory_prob
        long_term_memory_multiplier = 0.1
        self.words[word_id].long_term_recall_prob = min(1., long_term_memory_multiplier * short_term_memory_prob)


# if __name__ == '__main__':
#     num_words = 100
#     word_to_learn = np.arange(num_words)
#     native_language_equivelent = np.arange(num_words)
#
#     artificial_learner = artificial_learner(num_words, foreign_word_to_learn=word_to_learn,
#                                         native_language_equivelent=native_language_equivelent)
#
#     cumilative_time_delta = pd.to_datetime('3/7/2021')
#
#     for i in range(50):
#         cumilative_time_delta += timedelta(hours=1, minutes=1)
#         artificial_learner.study_word(0)
#         guess_correct_word = artificial_learner.update_word_understanding(0, cumilative_time_delta)
#         print(guess_correct_word)
#
#     # artificial_learner.degrade_short_term_memory(0)
#     #
#     # artificial_learner.study_word(0)
