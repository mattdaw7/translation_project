import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize

def generate_data():
    b = 0.07323778
    c = 0.12056247
    main_learning_function = lambda x: -b * (x - 1) * (x + c) + x
    observations = []
    states = [0]
    for i in range(100):
        observations.append(np.random.binomial(1, states[-1], 1)[0])
        states.append(main_learning_function(states[-1]))
    observations.append(np.random.binomial(1, states[-1], 1)[0])

    return observations, states

class baum_walch:
    def __init__(self):
        self.paraboloid_hyperparamter = 60

    def distribution_centering(self, a_matrix):
        results = []
        for row in a_matrix:
            max = np.argmax(row)
            spread = min(max, len(row) - max)
            spread = min(spread, 3)
            values = row[max - spread:max + spread + 1]
            ruler = np.arange(len(values)) - len(values) // 2
            results.append((np.sum((values @ ruler)) / np.sum(values) + max) * (1 / (len(row) - 1)))
        return results

    def construct_a_matrix(self, parameters, x_space):

        def kernel_function(center, x1):
            # if x1 < center:
            #     b_s = (x1 - center + .3 / np.sqrt(self.paraboloid_hyperparamter))
            # else:
            #     b_s = -1*(x1 - center - 1 / np.sqrt(self.paraboloid_hyperparamter))
            # if b_s < 0 or x1 < 0 or 1 < x1:
            #     return 0
            b_s = 1
            a_s = -(x1 - center) ** 2 * self.paraboloid_hyperparamter + 1
            if a_s < 0:
                return 0
            return a_s * b_s

        def a(x0, x1, parameters):
            center = -parameters[0] * (x0 - 1) * (x0 + parameters[1]) + x0
            parabaoloid_function = max(0, kernel_function(center, x1))
            return parabaoloid_function

        a(0,0,parameters)

        a_matrix = np.array([[a(x0, x1, parameters) for x1 in x_space] for x0 in x_space])
        # a_matrix = (a_matrix.T / np.sum(a_matrix, axis=1)).T

        # center_func = lambda x: -parameters[0] * (x - 1) * (x + parameters[1]) + x
        #
        # y = center_func(x_space)
        # print(y)
        #
        # result = distribution_centering(a_matrix)
        #
        # bad_estimate = np.argmax(a_matrix,axis=1)*(1 / (len(result) - 1))
        #
        # bad_residual = np.abs(y - bad_estimate)
        #
        # updated_residual = np.abs(result - y)
        #
        # print(result)

        return a_matrix

    def estimate_a_parameters(self, a, x_space, learning_parameters):
        ruler = np.arange(a.shape[1])
        median_indexes = (np.round(((a @ ruler) / np.sum(a, axis=1)))).astype(int)

        median_indexes = np.argmax(a, axis=1)

        inputs = x_space

        outputs = self.distribution_centering(a)

        print(outputs)

        def function_to_minimize(parameters):
            estimations = -parameters[0] * (x_space - 1) * (x_space + parameters[1]) + x_space

            return np.sum(abs(estimations - outputs))

        res = minimize(function_to_minimize, learning_parameters, tol=1e-8)

        return res.x

    def forward(self, V, a, b, initial_distribution):
        alpha = np.zeros((V.shape[0], a.shape[0]))
        alpha[0, :] = initial_distribution * b[:, V[0]]

        alpha[0] /= np.sum(alpha[0])

        for t in range(1, V.shape[0]):
            for j in range(a.shape[0]):
                # Matrix Computation Steps
                #                  ((1x2) . (1x2))      *     (1)
                #                        (1)            *     (1)
                alpha[t, j] = alpha[t - 1] @ a[:, j] * b[j, V[t]]
            alpha[t] /= np.sum(alpha[t])

        return alpha

    def backward(self, V, a, b):
        beta = np.zeros((V.shape[0], a.shape[0]))

        # setting beta(T) = 1
        beta[V.shape[0] - 1] = np.ones((a.shape[0]))

        beta[V.shape[0] - 1] /= np.sum(beta[V.shape[0] - 1])

        # Loop in backward way from T-1 to
        # Due to python indexing the actual loop will be T-2 to 0
        for t in range(V.shape[0] - 2, -1, -1):
            for j in range(a.shape[0]):
                beta[t, j] = (beta[t + 1] * b[:, V[t + 1]]) @ a[j, :]

                # get distribition estimation (beta) for time stamp
                #
            beta[t] /= np.sum(beta[t])

        return beta

    def estimate(self, observations, learning_parameters, initial_distribution, n_iter=300):

        x_space_size = len(initial_distribution)
        x_space = np.linspace(0, 1, x_space_size)

        a_matrix = self.construct_a_matrix(learning_parameters, x_space)
        b_matrix = np.array([1-x_space, x_space]).T

        self.estimate_a_parameters(a_matrix, x_space, learning_parameters)

        M = a_matrix.shape[0]
        T = len(observations)

        change_in_parameters = 600
        n = 0
        while n < n_iter and change_in_parameters > 0.0001:
            alpha = self.forward(observations, a_matrix, b_matrix, initial_distribution)
            beta = self.backward(observations, a_matrix, b_matrix)

            xi = np.zeros((M, M, T - 1))
            for t in range(T - 1):
                # joint probab of observed data up to time t @ transition prob * emisssion prob as t+1 @
                # joint probab of observed data from time t+1
                denominator = (alpha[t, :].T @ a_matrix * b_matrix[:, observations[t + 1]].T) @ beta[t + 1, :]
                for i in range(M):
                    numerator = alpha[t, i] * a_matrix[i, :] * b_matrix[:, observations[t + 1]].T * beta[t + 1, :].T
                    xi[i, :, t] = numerator / denominator

            gamma = np.sum(xi, axis=1)
            ### maximization step
            new_a_matrix = np.sum(xi, 2) / np.sum(gamma, axis=1).reshape((-1, 1))

            nan_rows = np.unique(np.where(new_a_matrix != new_a_matrix)[0])

            a_matrix = new_a_matrix

            new_learning_parameters = self.estimate_a_parameters(a_matrix, x_space, learning_parameters)
            change_in_parameters = np.linalg.norm(new_learning_parameters - learning_parameters)
            learning_parameters = new_learning_parameters
            print(learning_parameters, change_in_parameters)
            a_matrix = self.construct_a_matrix(learning_parameters, x_space)
            n += 1
        return learning_parameters

def function(x, parameters):
    main_learning_function = lambda x: -parameters[0] * (x - 1) * (x + parameters[1]) + x

    return main_learning_function(x)


if __name__ == '__main__':
    observation, states = generate_data()
    initial_distribution = np.zeros(101)
    initial_distribution[0] += 1
    estimator = baum_walch()
    observation = np.array(observation)
    initial_parameters = np.array([1, .12])

    new_parameters = estimator.estimate(observation, initial_parameters, initial_distribution)
    x_space = np.linspace(0, 1, 101)

    actual_parameter = np.array([0.07323778, 0.12056247])
    y1 = function(x_space, new_parameters)

    y2 = function(x_space, actual_parameter)

    y3 = function(x_space, initial_parameters)

    plt.xlabel("Iterations studied")
    plt.ylabel("How well student understands word")
    plt.plot(x_space, y1, label="estimated understanding")
    plt.plot(x_space, y2, label="real understanding")
    plt.plot(x_space, y3, label="initial estimate")

    plt.scatter(x_space, observation, label="observations")

    plt.legend()
    plt.show()




