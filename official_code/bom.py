import json
import re


def get_bom(filename='data/English_BoM.json'):
    """
    Returns the Book of Mormon json object
    """
    f = open(filename)
    raw = json.load(f)
    f.close()
    return raw


def get_bom_importance(filename='data/English_BoM.json'):
    """"
    Returns a dictionary of word keys and integer values, corresponding
    to how often a word is used in the Book of Mormon
    """
    raw = get_bom(filename)
    importance = dict()

    for book in raw['books']:
        for chapter in book['chapters']:
            for verse in chapter['verses']:
                for word in verse['text'].split():
                    without_punctuation = re.sub(r'[^\w\s]', '', word.lower())
                    if without_punctuation in importance:
                        importance[without_punctuation] += 1
                    else:
                        importance[without_punctuation] = 1

    return importance
