import datetime

class word:
    def __init__(self,id: int, foreign_word_to_learn=None, native_language_equivelent=None, native_language_definition=None, word_importance=None, short_knowledge=0, long_knowledge=0):
        """
            This class encodes all information relative to a student learning a specific word

            :param id: id of the word (as in the place it was added to memory)
            :param short_term_recall_prob: probability of recalling a word in short term
                This probability quickly increases increases with study but has a fast decay rate
            :param long_term_recall_prob: probability of recalling a word in long term
                This probability increases slowly with review but it decays very slowly
                If short_term_prob is very low will update minimially because the user doesn't know a word
                If short_term_prob is very high this will update minimally because the user is overly confident
            :param review_history: list of word_study_incidences for this particular word
            """
        # word id
        self.id = id
        # text of foreign word that we are trying to learn 
        self.foreign_word_to_learn = foreign_word_to_learn
        # text of native word equivalent
        self.native_language_equivelent = native_language_equivelent
        # text of definition of foreign word in native language
        self.native_language_definition = native_language_definition
        # utility gained by learning word
        self.word_importance = word_importance
        # a scaler between 0 and 1 that indicates how well we know this word in the short term
        self.short_term_recall_prob = short_knowledge
        # a scaler between 0 and 1 that indicates how well we know this word in the long terrm
        self.long_term_recall_prob = long_knowledge

        #  list of word_study_incident_current_session objects (should only include objects from current session)
        self.words_reviewed_in_current_session = []

        # list of word_study_incident_previous_session objects
        self.words_reviewed_in_previous_session = []

        #  list of word_study_incident_current_session objects (should include objects from all sessions ever)
        self.words_reviewed_previous_session_unformatted = []

        # this is the date time for our last study incident. If more than 3 hours have passed since our
        # last study incident then we start a new study session
        self.active_date = None


class word_study_incident_previous_session:
    def __init__(self, date, total_guesses, correct_guesses, short_term_average, long_term_average):
        """
        this object is an accumilation of everything that happened in an individual study session
        date is always a datetime

        If this object is being put into parent_learner_object then everything else is a dictionary with the key being word_id
        If this object is just going into the word class then all of these are just single numbers

        :param date: date of when study session happened
        :param total_guesses: number of times this word was guessed during session
        :param correct_guesses: number of times word was guessed correctly
        :param short_term_average: average of short term knowledge of word throughout session
        :param long_term_average: average of long term knowledge of word throughout session
        """
        self.date = date
        self.total_guesses = total_guesses
        self.correct_guesses = correct_guesses
        self.short_term_average = short_term_average
        self.long_term_average = long_term_average

class word_study_incident_current_session:
    def __init__(self, word_id, datetime, correct_guess, time_since_last_instance, short_term, long_term):
        """
        this object has all the information generated from studying one word
        :param word_id: id of word studied
        :param datetime: time stamp of when studying happened
        :param correct_guess: if the user's guess about the word was right (boolean)
        :param time_since_last_instance: time passed since last word studied in current session (otherwise we set it to zero time)
        :param short_term: estimate (or actual if in artificial learner) short term understanding of word
        :param long_term:estimate (or actual if in artificial learner) long term understanding of word
        """
        self.id = word_id
        self.datetime = datetime
        self.time_since_last_instance = time_since_last_instance
        self.correct_guess = correct_guess
        self.short_term = short_term
        self.long_term = long_term
