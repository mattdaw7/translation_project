import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
from datetime import timedelta

from word_class import word
from word_class import word_study_incident_previous_session
from word_class import word_study_incident_current_session

from parent_learner_object import parent_learner_object

class learner_estimator(parent_learner_object):
    def __init__(self, num_words, foreign_word_to_learn=None, native_language_equivelent=None, native_language_definition=None, word_importance_score=None, hours_to_break_session=1.5):
        """
        This class acts to estimate what our learner knows and predict what study word would best improve the
        student's learning
        :param num_words: number of words in dictionary
        foreign_word_to_learn: foreign word we wish to learn in it's foreign language
        native_language_equivelent: translation of word in user's target language
        native_language_definition: definition of foreign_word_to_learn typed out in user's native language
        word_importance_score: score value of utility gained in learning word
        hours_to_break_session: if this much time elapses between word study incidences then we start a new session
        """
        parent_learner_object.__init__(self, num_words=num_words, foreign_word_to_learn=foreign_word_to_learn, native_language_equivelent=native_language_equivelent, native_language_definition=native_language_definition, word_importance_score=word_importance_score, hours_to_break_session=hours_to_break_session)

    def degrade_short_term_memory(self, word_id):
        """
        This function will degrade estimate of our short term memory of a word given how long it has been since we've studied it
        :param word_id: word id of word to degrade our memory for
        :return: None
        """
        # this is all the informaiton you really need
        words_reviewed_in_current_session = self.words[word_id].words_reviewed_in_current_session
        words_reviewed_in_previous_session = self.words[word_id].words_reviewed_in_previous_session

        # you may also just concatenate this list to words_reviewed_in_current_session if that's easier to work with
        words_reviewed_previous_session_unformatted = self.words[word_id].words_reviewed_previous_session_unformatted

        # this is the information formatted to nicer number arrays without anything packed into custom objects
        constructed_states, time_studying_word, correct_guesses = self.get_state_information(word_id, use_only_current_session=True)

        ### TODO do the math to degreade our estimate of how well we know a word, note this function may be completely redundant
        # if this is done with the update_word_understanding_estimate function

    def get_word_to_sample(self):
        """
        This function will determine what word to have a user study next

        To determine the word, the function must consider how well we think a student knows a word in the short term,
        how long we think they know it in the long term, and the importance of the word itself

        :return: id of word to study next
        """
        # this information is only the information from current state
        constructed_states, unique_ids, time_studying_word, correct_guesses = self.get_state_information(use_only_current_session=True)

        info_previous_sessions = self.words_reviewed_in_previous_session
        info_current_session = self.words_reviewed_in_current_session
        info_previous_sessions_unstructured = self.words_reviewed_previous_session_unformatted

        ### TODO, do the math to estimate the expected gain for studying each word then return the word is expected to \
        # most increase our utility (utility being the produce of expected learning and word_import_score

    def update_word_understanding_estimate(self, word_id, user_guessed_correct):
        """
        this function updates our estimated states of how well a user knows a word given that they study it and get this result

        :param word_id: id of word to update our understanding for given that we just studied it
        :param user_guessed_correct: whether the user successfully guessed the word correctly
        :return: None
        """
        # this is all the informaiton you really need
        words_reviewed_in_current_session = self.words[word_id].words_reviewed_in_current_session
        words_reviewed_in_previous_session = self.words[word_id].words_reviewed_in_previous_session

        # you may also just concatenate this list to words_reviewed_in_current_session if that's easier to work with
        words_reviewed_previous_session_unformatted = self.words[word_id].words_reviewed_previous_session_unformatted

        # this is the information formatted to nicer number arrays without anything packed into custom objects
        constructed_states, time_studying_word, correct_guesses = self.get_state_information(word_id, use_only_current_session=True)

        ### TODO do the math to update our estimate for how well a know understands the word given the results of this study instance



if __name__ == '__main__':
    num_words = 100
    word_to_learn = np.arange(num_words)
    native_language_equivelent = np.arange(num_words)

    state_estimator = learner_estimator(num_words, foreign_word_to_learn=word_to_learn, native_language_equivelent=native_language_equivelent)

    cumilative_time_delta = pd.to_datetime('3/7/2021')

    for i in range(5):
        cumilative_time_delta += timedelta(hours=1, minutes=1)
        state_estimator.update_word_understanding(0, False, cumilative_time_delta)

    state_estimator.get_word_to_sample()

    state_estimator.degrade_short_term_memory(0)

    state_estimator.update_word_understanding_estimate(0, True)

