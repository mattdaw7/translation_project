import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
from datetime import timedelta

from word_class import word
from word_class import word_study_incident_previous_session
from word_class import word_study_incident_current_session


class parent_learner_object:
    def __init__(self, num_words, foreign_word_to_learn=None, native_language_equivelent=None, native_language_definition=None, word_importance_score=None, hours_to_break_session=1.5):
        """
        This class acts to estimate what our learner knows and predict what study word would best improve the
        student's learning
        :param num_words: number of words in dictionary
        foreign_word_to_learn: foreign word we wish to learn in it's foreign language
        native_language_equivelent: translation of word in user's target language
        native_language_definition: definition of foreign_word_to_learn typed out in user's native language
        word_importance_score: score value of utility gained in learning word
        hours_to_break_session: if this much time elapses between word study incidences then we start a new session
        """

        # this is a completely artificial function to assign a word important to arbitrary values
        # we only use it to initialize word importance for testing until we can get a better initialization with real data
        word_importance_function = lambda x: 20*(x/40+1)**-2

        # we set this variable globally
        self.hours_to_break_session = hours_to_break_session

        # this list is a list of word objects. Each word object controls the state of
        # our estimate for how well we know each word
        self.words = {}
        for i in range(num_words):
            # we check if these values are set by function inputs and if they are we get them prepared to pass
            # into the new word we are creating
            if foreign_word_to_learn is None:
                foreign_word_to = None
            else:
                foreign_word_to = foreign_word_to_learn[i]
            if native_language_equivelent is None:
                native_language_eq = None
            else:
                native_language_eq = native_language_equivelent[i]

            if native_language_definition is None:
                native_language_def = None
            else:
                native_language_def = native_language_definition[i]

            if word_importance_score is None:
                word_import_score = word_importance_function(i)
            else:
                word_import_score = word_importance_score[i]

            # once we set our words here this list practically speaking is treated as immutable as we can not add or take away new words
            # we are allowed to change our understanding of the words though, of course
            self.words[i] = word(i, foreign_word_to, native_language_eq, native_language_def, word_import_score, 0, 0)


        # list of word_study_incident_current_session objects
        self.words_reviewed_in_current_session = []
        # list of word_study_incident_previous_session objects
        self.words_reviewed_in_previous_session = []

        self.words_reviewed_previous_session_unformatted = []

        # this is the date time for our last study incident. If more than 3 hours have passed since our
        # last study incident then we start a new study session
        self.active_date = None

    def convert_current_session_to_previous(self):
        # we use the middle date as a proxy for when the study session was
        #   (I suppose a mean would also be appropriate here but I can't really think of a strong reason why it would
        #    be better than the mode or middle of times sampled, plust mode is easier in this case)
        middle_date = self.words_reviewed_in_current_session[
            len(self.words_reviewed_in_current_session) // 2].datetime.date()

        # these will be the variables we need to populate a previous session object
        # I think there names are self-explanatory
        correct_guesses = {}
        total_guesses = {}
        short_term_average = {}
        long_term_average = {}

        for study_incident in self.words_reviewed_in_current_session:
            # this maintains the expanded structure of current session put into the past
            # in the math, I think this should not be used as words studied in current session should
            # be handled very differently from words studied in current session, but that's just my intuition
            self.words_reviewed_previous_session_unformatted.append(study_incident)

            # if we have not encountered this word in our current session then we make new entries in our dictionaries
            if not(study_incident.id in correct_guesses.keys()):
                correct_guesses[study_incident.id] = study_incident.correct_guess
                total_guesses[study_incident.id] = 1
                short_term_average[study_incident.id] = [study_incident.short_term]
                long_term_average[study_incident.id] = [study_incident.long_term]
            # otherwise we just update the values in our dictionaries
            else:
                correct_guesses[study_incident.id] += study_incident.correct_guess
                total_guesses[study_incident.id] += 1
                short_term_average[study_incident.id].append(study_incident.short_term)
                long_term_average[study_incident.id].append(study_incident.long_term)

        # once the dictionaries are compiled, we convert the short_term_average and long_term_average lists into means
        for id in short_term_average.keys():
            short_term_average[id] = np.mean(short_term_average[id])
            long_term_average[id] = np.mean(long_term_average[id])

            # we repackage the word_study_incident_previous_session object for the word class
            # ie, we don't use a dictionary for the individual values
            self.words[id].words_reviewed_in_previous_session.append(word_study_incident_previous_session(middle_date[id], total_guesses[id], correct_guesses[id], short_term_average[id], long_term_average[id]))

            # we copy the words_reviewed_in_current_session to long term storage (ie, words_reviewed_previous_session_unformatted)
            self.words[id].words_reviewed_previous_session_unformatted += self.words[id].words_reviewed_in_current_session
            # empty words_reviewed_in_current_session
            self.words[id].words_reviewed_in_current_session = []

        # we crate a new word_study_incident_previous_session object for words_reviewed_in_previous_session
        self.words_reviewed_in_previous_session.append(
            word_study_incident_previous_session(middle_date, total_guesses, correct_guesses, short_term_average,
                                                 long_term_average))

        # empty words_reviewed_in_current_session
        self.words_reviewed_in_current_session = []


    def get_state_information(self, word_id=None, use_only_current_session=True):
        """
        this funciton gets all information related to each state of understanding the word

        word_id if not none will make is that only information related to a specific word will be returned

        :return:
        constructed_states: this is a two dimensional array with shape with the rows corresponding to word_ids
        and the columns corresponding to the study instance. The array then has the shape
        [num_words_studied_in_session, num_studies_in_session]
        every column i of the matrix is zero except for on the row_id that was studied at time step i

        unique_ids: these are the word ids that correspond to the row of the constructed_states matrix

        time_studying_word: this is a list of how long was spent on each study instance by the user

        correct_guesses: if one on entry i then study instance i was correctly guessed, else zero if not correctly guessed
        """

        session_word_list = self.words_reviewed_in_current_session
        if not(use_only_current_session):
            session_word_list = self.words_reviewed_previous_session_unformatted

        study_ids = []
        time_studying_word = []
        correct_guesses = []
        for study_incident in session_word_list:
            study_ids.append(study_incident.id)
            time_studying_word.append(study_incident.time_since_last_instance)
            correct_guesses.append(study_incident.correct_guess)

        unique_ids = np.unique(study_ids)

        constructed_states = np.zeros((len(unique_ids), len(study_ids)))
        for i, id in enumerate(unique_ids):
            overlap_indicies = np.where(id == study_ids)

            constructed_states[i][overlap_indicies] = 1

        if not(word_id is None):
            if word_id in unique_ids:
                index_in_list = np.where(unique_ids == word_id)[0][0]
                return  constructed_states[index_in_list], time_studying_word, correct_guesses
            else:
                return np.zeros(constructed_states.shape[0]), time_studying_word, correct_guesses

        return constructed_states, unique_ids, time_studying_word, correct_guesses


    def update_word_understanding(self, word_id: int, timestamp, guess_correct_word=None):
        """
        function to update understanding of word given results of reviewing it
        :param word_id:
        :get_correct_word: if word studied
        :return: None
        """

        if guess_correct_word is None:
            guess_correct_word = np.random.binomial(1, self.words[word_id].short_term_recall_prob, 1)[0]

            # guess_correct_word = np.random.

        # the second if statement will throw an error if we don't check against this
        if not(self.active_date is None):
            # if enough time has passed from the last time we studied a word and there is at least one item in self.words_reviewed_in_current_session
            # then we need to empty self.words_reviewed_in_current_session and move it's contents to longer term storage
            # note, checking len(self.words_reviewed_in_current_session) is probably a redundant operation but sometimes
            # redundancies are good
            if (timestamp - self.active_date).seconds / 3600 > self.hours_to_break_session and len(self.words_reviewed_in_current_session) > 0:
                self.convert_current_session_to_previous()

                # call the degrade_short_term_memory that is either defined in artificial_learner or main depending on where this
                # class was inherited to
                self.degrade_short_term_memory(word_id)

        # update the latest active time stamp
        self.active_date = timestamp

        # find time since we last did a review
        time_since_last_review = self.active_date - self.active_date
        if len(self.words_reviewed_in_current_session) > 0:
            time_since_last_review = timestamp - self.words_reviewed_in_current_session[-1].datetime

        # create a new study instance for the word we just now quized on
        new_word_study_instance = word_study_incident_current_session(word_id, timestamp, guess_correct_word,
                                                time_since_last_review,
                                                self.words[word_id].short_term_recall_prob,
                                                self.words[word_id].long_term_recall_prob)


        # copy that study instance object into our global words_reviewed_in_current_session list and to the
        # words_reviewed_in_current_session list for our specific word
        self.words_reviewed_in_current_session.append(new_word_study_instance)
        self.words[word_id].words_reviewed_in_current_session.append(new_word_study_instance)

        return guess_correct_word
