import numpy as np
import matplotlib.pyplot as plt
from data_generator import data_generator
from scipy.stats import norm
from scipy.integrate import quad

class continous_baum_welch:
    def __init__(self, b=0.1, c=0.05, d=0.01):
        self.b = b
        self.c = c
        self.d = d
        self.learning_function = lambda x, b, c: -b*(x-1)*(x+c) + x
        self.universal_std = 0.05

    def state_to_obs_prob(self, x, b, c, d):
        learning_improvement = -b*(x-1)*(x+c)
        return learning_improvement

    def forward_pass(self,obs,b,c,d,initial_distribution):
        if obs[0] == 0:
            initial_alpha = lambda x: initial_distribution(x) * (1-x)
        else:
            initial_alpha = lambda x: initial_distribution(x) * (x)

        alpha_functions = [initial_alpha]

        for i in range(len(obs)-1):
            fun_to_integrate = lambda x: alpha_functions[i](x) * norm.pdf(x, self.learning_function(x, b, c),
                                                                          self.universal_std)
            integrated_value, _ = quad(fun_to_integrate, 0, 1)

            if obs[i+1] == 0:
                new_alpha = lambda x: (1-x)*integrated_value
            else:
                new_alpha = lambda x: x * integrated_value

            alpha_functions.append(new_alpha)





    def baum_welch(self, obs, b, c, d, initial_distribution):
        self.forward_pass(obs, b, c, d, initial_distribution)

if __name__ == '__main__':
    generator = data_generator()
    collected_runs = []

    for i in range(50):
        user_guesses, user_understanding = generator.get_data_run()
        collected_runs.append(user_guesses)

    collected_runs = np.array(collected_runs)


    learner = continous_baum_welch()

    s = 0.000001
    initial_distribution = lambda x: (s ** x - s) / (1 - s)

    b_initial = 0.001
    c_initial = 0.2
    d_initial = 0.01

    learner.baum_welch(collected_runs[0], b_initial, c_initial, d_initial, initial_distribution)



