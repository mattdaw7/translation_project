from keras.datasets import fashion_mnist
from sklearn.model_selection import GridSearchCV
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm, datasets
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report

if __name__ == '__main__':
    (x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()

    n = 30

    x_train = x_train[:n]

    x_train = x_train.reshape(-1, 28*28)

    y_train = y_train[:n]

    # import some data to play with
    # X = x_train[:, :2]  # we only take the first two features. We could
    X = x_train
    # avoid this ugly slicing by using a two-dim dataset
    Y = y_train


    def my_kernel(X, Y):
        """
        We create a custom kernel:

                     (2  0)
        k(X, Y) = X  (    ) Y.T
                     (0  1)
        """
        M = np.eye(len(X[0]))
        return np.dot(np.dot(X, M), Y.T)


    clf = svm.SVC(kernel="linear", degree=2)
    clf.fit(X, Y)
    clf.predict(X)

    clf = svm.SVC(kernel="poly", degree=2, gamma=10, coef0=2)
    clf.fit(X, Y)
    clf.predict(X)

    clf = svm.SVC(gamma=10)
    clf.fit(X, Y)
    clf.predict(X)

    svc = svm.SVC()
    parameters = {'kernel': ('linear', 'rbf', 'poly'), 'gamma': np.linspace(1, 10, 2), 'coef0': np.linspace(0, 10, 2),'degree': [2, 3, 4, 5]}
    # parameters = {'coef0':np.linspace(0,10,10)}
    grid = GridSearchCV(svc, parameters)
    grid.fit(x_train, y_train)
    print(grid.best_params_)
    print(grid.best_estimator_)
    grid_predictions = grid.predict(x_train)
    print(classification_report(y_train, grid_predictions))
