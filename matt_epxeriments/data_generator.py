import numpy as np
import matplotlib.pyplot as plt

class data_generator:
    def __init__(self):
        self.word_knowledge = 0

        # be is a positive number
        b = .01

        # c is  a number strictly greater than zero but generally should be less than one
        c = .1

        self.main_learning_function = lambda x: -b*(x-1)*(x+c)

        # d is a number strictly greater than zero. It singifies how fast a leraner forgets a word given that they
        # know it "perfectly"
            # note, this value can very very small but do not make it zero or the learner will never forget anything
        d = .001

        self.word_update_func = lambda x: d**x

    def get_state(self):
        guess = np.random.binomial(1, self.word_knowledge, 1)[0]
        update = self.word_update_func(guess)

    def get_data_run(self,data_to_collect=300):
        iniital_understanding = 0

        user_guesses = []
        user_understanding = [0]

        for i in range(data_to_collect):
            new_guess = np.random.binomial(1,user_understanding[-1],1)[0]
            user_guesses.append(new_guess)
            user_understanding.append( user_understanding[-1]+self.main_learning_function(user_understanding[-1]))
            if user_understanding[-1] > 1:
                user_understanding[-1] = 1

        return user_guesses, user_understanding
