import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize

def generate_data():
    b = .08
    c = .1
    main_learning_function = lambda x: -b * (x - 1) * (x + c) + x
    observations = []
    states = [0]
    for i in range(100):
        observations.append(np.random.binomial(1, states[-1], 1)[0])
        states.append(main_learning_function(states[-1]))
    observations.append(np.random.binomial(1, states[-1], 1)[0])

    return observations, states

class baun_walch:
    def __init__(self):
        self.paraboloid_hyperparamter = 64


if __name__ == '__main__':
    observation, states = generate_data()
    initial_distribution = np.zeros(100)
    initial_distribution[0] += 1


    estimator = baum_walch()
    observation = np.array(observation)
    estimator.estimate(observation, np.array([.1, .12]), initial_distribution)
    print("think")
    # plt.plot(states)
    # plt.show()
