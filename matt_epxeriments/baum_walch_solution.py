# import pandas as pd
# import numpy as np
# from hmmlearn import hmm
# from data_generator import data_generator
# import matplotlib.pyplot as plt
#
# def forward(V, a, b, initial_distribution):
#     alpha = np.zeros((V.shape[0], a.shape[0]))
#     alpha[0, :] = initial_distribution * b[:, V[0]]
#
#     for t in range(1, V.shape[0]):
#         for j in range(a.shape[0]):
#             # Matrix Computation Steps
#             #                  ((1x2) . (1x2))      *     (1)
#             #                        (1)            *     (1)
#             alpha[t, j] = alpha[t - 1] @ a[:, j] * b[j, V[t]]
#
#     return alpha
#
# def backward(V, a, b):
#     beta = np.zeros((V.shape[0], a.shape[0]))
#
#     # setting beta(T) = 1
#     beta[V.shape[0] - 1] = np.ones((a.shape[0]))
#
#     # Loop in backward way from T-1 to
#     # Due to python indexing the actual loop will be T-2 to 0
#     for t in range(V.shape[0] - 2, -1, -1):
#         for j in range(a.shape[0]):
#             beta[t, j] = (beta[t + 1] * b[:, V[t + 1]]) @ a[j, :]
#
#             # get distribition estimation (beta) for time stamp
#             #
#
#     return beta
#
#
#
# def baum_welch(V, a, b, initial_distribution, n_iter=100):
#     M = a.shape[0]
#     T = len(V)
#
#     for n in range(n_iter):
#         ###estimation step
#         alpha = forward(V, a, b, initial_distribution)
#         beta = backward(V, a, b)
#
#         xi = np.zeros((M, M, T - 1))
#         for t in range(T - 1):
#             # joint probab of observed data up to time t @ transition prob * emisssion prob as t+1 @
#             # joint probab of observed data from time t+1
#             denominator = (alpha[t, :].T @ a * b[:, V[t + 1]].T) @ beta[t + 1, :]
#             for i in range(M):
#                 numerator = alpha[t, i] * a[i, :] * b[:, V[t + 1]].T * beta[t + 1, :].T
#                 xi[i, :, t] = numerator / denominator
#
#         gamma = np.sum(xi, axis=1)
#         ### maximization step
#         a = np.sum(xi, 2) / np.sum(gamma, axis=1).reshape((-1, 1))
#
#         # Add additional T'th element in gamma
#         gamma = np.hstack((gamma, np.sum(xi[:, :, T - 2], axis=0).reshape((-1, 1))))
#
#         K = b.shape[1]
#         denominator = np.sum(gamma, axis=1)
#         for l in range(K):
#             b[:, l] = np.sum(gamma[:, V == l], axis=1)
#
#         b = np.divide(b, denominator.reshape((-1, 1)))
#
#     return a, b
#
# def make_out_of_samples(a_model, distribtun, samples):
#
#     estimations = []
#     base_ruler = np.arange(len(distribtun)) / (len(distribtun)-1)
#     # base_ruler = base_ruler / np.sum(base_ruler)
#     for i in range(samples):
#         numerator = np.sum(distribtun)
#         distance = np.sum(base_ruler * distribtun) / np.sum(distribtun)
#         estimations.append(distance)
#         distribtun = a_model @ distribtun
#
#         distribtun = distribtun / np.sum(distribtun)
#
#         a_model[0,:] @ distribtun
#
#     return estimations
#
# if __name__ == '__main__':
#     learner = data_generator()
#     user_guesses, user_understanding = learner.get_data_run()
#
#     V = np.array(user_guesses)
#
#     hidden_states = 45
#
#     a = np.ones((hidden_states, hidden_states))
#     a = a / np.sum(a, axis=1)
#
#     initial_states = [.05,.08]
#
#     # Emission Probabilities
#     b = np.ones((hidden_states, 2))
#     b = b / np.sum(b, axis=1).reshape((-1, 1))
#
#     # Equal Probabilities for the initial distribution
#     initial_distribution = np.zeros(hidden_states)
#     initial_distribution[0] += 1
#     initial_distribution = initial_distribution / np.sum(initial_distribution)
#
#     n_iter = 100
#     a_model, b_model = baum_welch(V.copy(), a.copy(), b.copy(), initial_distribution.copy(), n_iter=n_iter)
#     print(f'Custom model A is \n{a_model} \n \nCustom model B is \n{b_model}')
#
#     model = hmm.MultinomialHMM(n_components=hidden_states, n_iter=n_iter, init_params="", params="te", tol=0)
#     model.startprob_ = initial_distribution
#     model.transmat_ = a
#     model.emissionprob_ = b
#
#     model.fit([V])
#     a_model, b_model = model.transmat_, model.emissionprob_
#
#     estimations = make_out_of_samples(a_model, initial_distribution, len(V))
#
#     plt.xlabel("Times quizzed on word")
#     plt.ylabel("Understanding of word")
#
#     plt.title("Estimate of Word Understanding")
#
#     plt.plot(estimations, label="estimated user understanding")
#     plt.plot(user_understanding, label="real userr understanding")
#     plt.legend()
#     plt.show()
#




import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize

def generate_data():
    b = 0.07323778
    c = 0.12056247
    main_learning_function = lambda x: -b * (x - 1) * (x + c) + x
    observations = []
    states = [0]
    for i in range(100):
        observations.append(np.random.binomial(1, states[-1], 1)[0])
        states.append(main_learning_function(states[-1]))
    observations.append(np.random.binomial(1, states[-1], 1)[0])

    return observations, states

class baum_walch:
    def __init__(self):
        self.paraboloid_hyperparamter = 600

    def distribution_centering(self, a_matrix):
        results = []
        for row in a_matrix:
            max = np.argmax(row)
            spread = min(max, len(row) - max)
            spread = min(spread, 3)
            values = row[max - spread:max + spread + 1]
            ruler = np.arange(len(values)) - len(values) // 2
            results.append((np.sum((values @ ruler)) / np.sum(values) + max) * (1 / (len(row) - 1)))
        return results

    def construct_a_matrix(self, parameters, x_space):

        def kernel_function(center, x1):
            # if x1 < center:
            #     b_s = (x1 - center + .3 / np.sqrt(self.paraboloid_hyperparamter))
            # else:
            #     b_s = -1*(x1 - center - 1 / np.sqrt(self.paraboloid_hyperparamter))
            # if b_s < 0 or x1 < 0 or 1 < x1:
            #     return 0
            b_s = 1
            a_s = -(x1 - center) ** 2 * self.paraboloid_hyperparamter + 1
            if a_s < 0:
                return 0
            return a_s * b_s

        def a(x0, x1, parameters):
            center = -parameters[0] * (x0 - 1) * (x0 + parameters[1]) + x0
            parabaoloid_function = max(0, kernel_function(center, x1))
            return parabaoloid_function

        a(0,0,parameters)

        a_matrix = np.array([[a(x0, x1, parameters) for x1 in x_space] for x0 in x_space])
        # a_matrix = (a_matrix.T / np.sum(a_matrix, axis=1)).T

        # center_func = lambda x: -parameters[0] * (x - 1) * (x + parameters[1]) + x
        #
        # y = center_func(x_space)
        # print(y)
        #
        # result = distribution_centering(a_matrix)
        #
        # bad_estimate = np.argmax(a_matrix,axis=1)*(1 / (len(result) - 1))
        #
        # bad_residual = np.abs(y - bad_estimate)
        #
        # updated_residual = np.abs(result - y)
        #
        # print(result)

        return a_matrix

    def estimate_a_parameters(self, a, x_space, learning_parameters):
        ruler = np.arange(a.shape[1])
        median_indexes = (np.round(((a @ ruler) / np.sum(a, axis=1)))).astype(int)

        median_indexes = np.argmax(a, axis=1)

        inputs = x_space

        outputs = self.distribution_centering(a)

        def function_to_minimize(parameters):
            estimations = -parameters[0] * (x_space - 1) * (x_space + parameters[1]) + x_space

            return np.sum(abs(estimations - outputs))

        res = minimize(function_to_minimize, learning_parameters, tol=1e-8)

        return res.x

    def forward(self, V, a, b, initial_distribution):
        alpha = np.zeros((V.shape[0], a.shape[0]))
        alpha[0, :] = initial_distribution * b[:, V[0]]

        for t in range(1, V.shape[0]):
            for j in range(a.shape[0]):
                # Matrix Computation Steps
                #                  ((1x2) . (1x2))      *     (1)
                #                        (1)            *     (1)
                alpha[t, j] = alpha[t - 1] @ a[:, j] * b[j, V[t]]
        return alpha

    def backward(self, V, a, b):
        beta = np.zeros((V.shape[0], a.shape[0]))

        # setting beta(T) = 1
        beta[V.shape[0] - 1] = np.ones((a.shape[0]))

        # Loop in backward way from T-1 to
        # Due to python indexing the actual loop will be T-2 to 0
        for t in range(V.shape[0] - 2, -1, -1):
            for j in range(a.shape[0]):
                beta[t, j] = (beta[t + 1] * b[:, V[t + 1]]) @ a[j, :]

                # get distribition estimation (beta) for time stamp
                #

        return beta

    def estimate(self, observations, learning_parameters, initial_distribution, n_iter=100):

        x_space_size = len(initial_distribution)
        x_space = np.linspace(0, 1, x_space_size)

        a_matrix = self.construct_a_matrix(learning_parameters, x_space)
        b_matrix = np.array([1-x_space, x_space]).T

        self.estimate_a_parameters(a_matrix, x_space, learning_parameters)

        M = a_matrix.shape[0]
        T = len(observations)

        change_in_parameters = 100
        n = 0
        while n < n_iter and change_in_parameters > 0.0001:
            summed_numerator = 0
            summed_denominator = 0
            for l in range(len(observations)):
                alpha = self.forward(observations[l], a_matrix, b_matrix, initial_distribution)
                beta = self.backward(observations[l], a_matrix, b_matrix)

                xi = np.zeros((M, M, T - 1))
                for t in range(T - 1):
                    # joint probab of observed data up to time t @ transition prob * emisssion prob as t+1 @
                    # joint probab of observed data from time t+1
                    denominator = (alpha[t, :].T @ a_matrix * b_matrix[:, observations[l,t + 1]].T) @ beta[t + 1, :]
                    for i in range(M):
                        numerator = alpha[t, i] * a_matrix[i, :] * b_matrix[:, observations[l,t + 1]].T * beta[t + 1, :].T
                        xi[i, :, t] = numerator / denominator

                gamma = np.sum(xi, axis=1)
                ### maximization step
                a_matrix = np.sum(xi, 2) / np.sum(gamma, axis=1).reshape((-1, 1))

                summed_numerator += np.sum(xi, 2)
                summed_denominator += np.sum(gamma, axis=1).reshape((-1, 1))

            a_matrix = summed_numerator/summed_denominator

            new_learning_parameters = self.estimate_a_parameters(a_matrix, x_space, learning_parameters)
            change_in_parameters = np.linalg.norm(new_learning_parameters - learning_parameters)
            learning_parameters = new_learning_parameters
            print(learning_parameters, change_in_parameters)
            a_matrix = self.construct_a_matrix(learning_parameters, x_space)
            n_iter += 1

        return learning_parameters

def function(x, parameters):
    main_learning_function = lambda x: -parameters[0] * (x - 1) * (x + parameters[1]) + x

    return main_learning_function(x)


if __name__ == '__main__':
    observation, states = generate_data()

    observation2, states = generate_data()

    observation3, states = generate_data()

    observation4, states = generate_data()

    stacked_observations = np.array([observation, observation2, observation3, observation4])

    initial_distribution = np.zeros(101)
    initial_distribution[0] += 1
    estimator = baum_walch()
    observation = np.array(observation)
    initial_parameters = np.array([.02, .1])

    new_parameters = estimator.estimate(stacked_observations, initial_parameters, initial_distribution)
    x_space = np.linspace(0, 1, 101)

    actual_parameter = np.array([0.07323778, 0.12056247])
    y1 = function(x_space, new_parameters)

    y2 = function(x_space, actual_parameter)

    fig, axs = plt.subplots(3)

    axs[0].plot(x_space, y1)
    axs[1].plot(x_space, y2)

    axs[2].plot(x_space, y1)
    axs[2].plot(x_space, y2)

    plt.show()




    # def estimate_a_parameters(self, a, x_space, learning_parameters):
    #     ruler = np.arange(a.shape[1])
    #     median_indexes = (np.round(((a @ ruler) / np.sum(a, axis=1)))).astype(int)
    #
    #     median_indexes = np.argmax(a, axis=1)
    #
    #     inputs = x_space
    #
    #     outputs = self.distribution_centering(a)
    #
    #     def function_to_minimize(parameters):
    #         estimations = -parameters[0] * (x_space - 1) * (x_space + parameters[1]) + x_space
    #         return np.sum(abs(estimations - outputs))
    #
    #     a_perturbing = np.linspace(0,.4,100)
    #     z = np.zeros([len(a_perturbing),len(a_perturbing)])
    #     for i, x in enumerate(a_perturbing):
    #         for j, y in enumerate(a_perturbing):
    #             z[i][j] = function_to_minimize([x, y])
    #
    #     X, Y = np.meshgrid(a_perturbing,a_perturbing)
    #
    #     plt.contour(a_perturbing, a_perturbing, z)
    #
    #     plt.show()
    #
    #     x, y = np.where(np.min(z) == z)
    #
    #     x = x[0]
    #     y = y[0]
    #
    #     return [x, y]