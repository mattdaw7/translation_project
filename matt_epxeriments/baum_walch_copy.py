import pandas as pd
import numpy as np
from hmmlearn import hmm
from data_generator import data_generator
import matplotlib.pyplot as plt

def forward(V, a, b, initial_distribution):
    alpha = np.zeros((V.shape[0], a.shape[0]))
    alpha[0, :] = initial_distribution * b[:, V[0]]

    for t in range(1, V.shape[0]):
        for j in range(a.shape[0]):
            # Matrix Computation Steps
            #                  ((1x2) . (1x2))      *     (1)
            #                        (1)            *     (1)
            alpha[t, j] = alpha[t - 1] @ a[:, j] * b[j, V[t]]

    return alpha

def backward(V, a, b):
    beta = np.zeros((V.shape[0], a.shape[0]))

    # setting beta(T) = 1
    beta[V.shape[0] - 1] = np.ones((a.shape[0]))

    # Loop in backward way from T-1 to
    # Due to python indexing the actual loop will be T-2 to 0
    for t in range(V.shape[0] - 2, -1, -1):
        for j in range(a.shape[0]):
            beta[t, j] = (beta[t + 1] * b[:, V[t + 1]]) @ a[j, :]

            # get distribition estimation (beta) for time stamp
            #

    return beta



def baum_welch(V, a, b, initial_distribution, n_iter=100):
    M = a.shape[0]
    T = len(V)

    for n in range(n_iter):
        ###estimation step
        alpha = forward(V, a, b, initial_distribution)
        beta = backward(V, a, b)

        xi = np.zeros((M, M, T - 1))
        for t in range(T - 1):
            # joint probab of observed data up to time t @ transition prob * emisssion prob as t+1 @
            # joint probab of observed data from time t+1
            denominator = (alpha[t, :].T @ a * b[:, V[t + 1]].T) @ beta[t + 1, :]
            for i in range(M):
                numerator = alpha[t, i] * a[i, :] * b[:, V[t + 1]].T * beta[t + 1, :].T
                xi[i, :, t] = numerator / denominator

        gamma = np.sum(xi, axis=1)
        ### maximization step
        a = np.sum(xi, 2) / np.sum(gamma, axis=1).reshape((-1, 1))

        # Add additional T'th element in gamma
        gamma = np.hstack((gamma, np.sum(xi[:, :, T - 2], axis=0).reshape((-1, 1))))

        K = b.shape[1]
        denominator = np.sum(gamma, axis=1)
        for l in range(K):
            b[:, l] = np.sum(gamma[:, V == l], axis=1)

        b = np.divide(b, denominator.reshape((-1, 1)))

    return a, b

def make_out_of_samples(a_model, distribtun, samples):

    estimations = []
    base_ruler = np.arange(len(distribtun)) / (len(distribtun)-1)
    # base_ruler = base_ruler / np.sum(base_ruler)
    for i in range(samples):
        numerator = np.sum(distribtun)
        distance = np.sum(base_ruler * distribtun) / np.sum(distribtun)
        estimations.append(distance)
        distribtun = a_model @ distribtun

        distribtun = distribtun / np.sum(distribtun)

        a_model[0,:] @ distribtun

    return estimations

if __name__ == '__main__':
    learner = data_generator()
    user_guesses, user_understanding = learner.get_data_run()

    V = np.array(user_guesses)

    hidden_states = 45

    a = np.ones((hidden_states, hidden_states))
    a = a / np.sum(a, axis=1)

    initial_states = [.05,.08]

    # Emission Probabilities
    b = np.ones((hidden_states, 2))
    b = b / np.sum(b, axis=1).reshape((-1, 1))

    # Equal Probabilities for the initial distribution
    initial_distribution = np.zeros(hidden_states)
    initial_distribution[0] += 1
    initial_distribution = initial_distribution / np.sum(initial_distribution)

    n_iter = 100
    a_model, b_model = baum_welch(V.copy(), a.copy(), b.copy(), initial_distribution.copy(), n_iter=n_iter)
    print(f'Custom model A is \n{a_model} \n \nCustom model B is \n{b_model}')

    # model = hmm.MultinomialHMM(n_components=hidden_states, n_iter=n_iter, init_params="", params="te", tol=0)
    # model.startprob_ = initial_distribution
    # model.transmat_ = a
    # model.emissionprob_ = b
    #
    # model.fit([V])
    # a_model, b_model = model.transmat_, model.emissionprob_
    #
    # estimations = make_out_of_samples(a_model, initial_distribution, len(V))
    #
    # plt.xlabel("Times quizzed on word")
    # plt.ylabel("Understanding of word")
    #
    # plt.title("Estimate of Word Understanding")
    #
    # plt.plot(estimations, label="estimated user understanding")
    # plt.plot(user_understanding, label="real userr understanding")
    # plt.legend()
    # plt.show()
    #
