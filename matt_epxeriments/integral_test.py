
from sympy import *
import numpy as np
if __name__ == '__main__':
    x1 = symbols('x1', real=True)
    x0 = symbols('x0', real=True)

    b = 1
    c = 0.01

    a_quad = -b
    b_quad = b-c*b+1
    c_quad = b*c+np.sqrt(1/64)-.25

    intersect_point = (-b_quad + np.sqrt(b_quad**2-4*a_quad*c_quad))/(2*a_quad)

    # x0 = intersect_point

    -1*np.sqrt(1/64) + .25

    first_form = -b * (x0 - 1) * (x0 + c) + x0

    # normalizing_coeff = 1/6-Piecewise( (((-8*(.125-first_form)**2*(8*(.125-first_form)-3))/3), first_form<.125 ), ((-8*(first_form-.875)**2*(8*(first_form-.875)-3))/3 , first_form>.875 ), (0,True) )

    x = .125

    area_integrateds = -40/3

    area_list = 1/6-Piecewise( (-(8*(.125-x)**2*(8*(.125-x)-3))/3, x<.125), (-(8*(.875-x)**2*(8*(.875-x)-3))/3, .875<x), (0,True) )

    # formula = Piecewise((0, x1 < 0), (0, x1 > 0), ( (area_list)*(-64*(x1-first_form)**2+1) , (first_form - 0.125 < x1) & (x1 < first_form + 0.125)), (0, True))

    # formula = Piecewise((0, x1 < 0), (0, x1 > 1),
                        # ((-64 * (x1 - first_form) ** 2 + 1), (.125 - first_form < x1) & (x1 < .125 + first_form)))

    lower_bound = (.125 - first_form)
    upper_bound = (.125 + first_form)
    formula = Piecewise( ( (-64*(x1-first_form)**2+1) , (0 < x1) & (x1 < .25) ), (0,True) )

    result = 1/area_list * integrate(formula, (x1, 0, .25))

    s = 0.001


