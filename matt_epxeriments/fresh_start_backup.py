import numpy as np
import matplotlib.pyplot as plt


def generate_data():
    b = .08
    c = .1
    main_learning_function = lambda x: -b * (x - 1) * (x + c) + x
    observations = []
    states = [0]
    for i in range(100):
        observations.append(np.random.binomial(1, states[-1], 1)[0])
        states.append(main_learning_function(states[-1]))
    observations.append(np.random.binomial(1, states[-1], 1)[0])

    return observations, states


class baum_walch:
    def __init__(self):
        self.paraboloid_hyperparamter = 64

    def construct_a_matrix(self, obersvations, parameters, x_space):

        def kernel_function(center, x1):
            a_s = -(x1 - center) ** 2 * self.paraboloid_hyperparamter + 1
            b_s = (x1 - center + 1 / np.sqrt(self.paraboloid_hyperparamter))
            return a_s * b_s

        def a(x0, x1, parameters):
            center = -parameters[0] * (x0 - 1) * (x0 + parameters[1]) + x0
            parabaoloid_function = max(0, kernel_function(center, x1))
            return parabaoloid_function

        a_matrix = np.array([[a(x0, x1, parameters) for x1 in x_space] for x0 in x_space])
        a_matrix = (a_matrix / np.sum(a_matrix, axis=0))

        return a_matrix

    def forward_pass(self, observations, parameters, initial_distribution, a_matrix):
        alpha = initial_distribution * self.b(self.x_space, observations[0])
        collected_alpha_values = [alpha]

        for o in range(len(observations) - 1):
            alpha = self.b(self.x_space, observations[o + 1]) * (a_matrix @ alpha)
            collected_alpha_values.append(alpha)

        return np.array(collected_alpha_values)

    def backward_pass(self, observations, parameters, initial_distribution, a_matrix):

        self.x_space_size = len(initial_distribution)
        self.x_space = np.linspace(0, 1, self.x_space_size)
        beta = [np.ones(self.x_space_size)]

        for t in range(self.x_space_size - 1, -1, -1):
            beta.append((a_matrix.T @ beta[-1]) * self.b(self.x_space, observations[t]))

        return np.array(beta[::-1])

    def estimate(self, observations, initial_parameters, initial_distribution, n_iter=100):

        self.x_space_size = len(initial_distribution)
        self.x_space = np.linspace(0, 1, self.x_space_size)

        a_matrix = self.construct_a_matrix(observations, initial_parameters, self.x_space)
        b_matrix = np.array([1 - self.x_space, self.x_space])

        for n in range(n_iter):
            alpha = self.forward(observations, a_matrix, b_matrix, initial_distribution)
            beta = self.backward(observations, a_matrix, b_matrix)

        alphas = self.forward_pass(observations, initial_parameters, initial_distribution, a_matrix)
        betas = self.backward_pass(observations, initial_parameters, initial_distribution, a_matrix)

        p_of_z = np.sum(alphas[-1])
        gamma = alphas * betas / p_of_z

        print("think")


if __name__ == '__main__':
    observation, states = generate_data()
    initial_distribution = np.zeros(100)
    initial_distribution[0] += 1
    estimator = baum_walch()
    estimator.estimate(observation, np.array([.1, .12]), initial_distribution)
    print("think")
    # plt.plot(states)
    # plt.show()

